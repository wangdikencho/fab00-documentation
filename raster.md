# Vectorizing a raster image
## software used: Inkscape
I opened my inkscape software and imported an image which is white and black in color. I couldn't find any image so I used the dragon image whcih was used by our mentor Fran.

### following are the steps to image vectorization
- Go to tool bar and select path.
- Select the trace bitmap.
- Inside the trace bitmap select single scan which is for single color image vectorization.
- Select the image and try to increase or decrease the brightness threshold to get your required vector image.
- Click on update and click apply only once.
- You will find changes with different brightness threshold like in the image shared below.



![](./img/temp/path.png)



I have tried with different brightness threshold and at the end i have selected only one and deleted the others.

I have to decide my image size so that the machine will cut my image without any issues therefore I unlocked the lock button which is right above scale margin and selected the unit to mm.
I have set my width to 210 mm and units converted to mm.
After that Click on the margin lock and select top =5, rest left,right and buttom will automatically change once u unlock the key.
Click on the Resize page to drawing once you finished making changes.
The image border size will get changed as I have shown in the image below.


![](./img/temp/size.png)


---

# Rasterization
## How to Rasterize from vector with better image quality to engrave it?

Go to file and select export png image.
In the export area select page and set width to 210 and units to mm.
Changing the dpi unit will automatically change the image width and height pixcels.

dpi 600 is ideal for many machine but if we wanted to print small circuit boards we have to set dpi more than 1000, so that every detail will be printed.
After that export your image by clicking the Export As button to your fabzero documentation folder.

![](./img/temp/raster.png)

---

