# How to make Double layer PCB board
To make a double sided pcb board, first open the schematic editor and start designing the circuit board. In my case I edited my previous circuit by just adding some components like battery, a switch, and 2 led. 
To calculate the resistance of the resistor use the formula ***V=IR***.
![](./img/KiCad/double.png)

Once added all the components, rename and assign footprints and values to the components. Then run ECC to make sure all the components are connected. 
![](./img/KiCad/ERC.png)

Open schematic PCB in board editor. Arrange the other components inside the battery. Change the battery's VCC and GND to bottom layer by selecting the them and enter F on keyboard to flip them to bottom layer. Click E on keyboard to edit the orientation of the components.
Then do routing . To route for the backside layer, go to Board Setup and select pre-defined sizes.
![route](./img/KiCad/route.png)
![](./img/KiCad/predefine.png)
```
Click V on key board to make hole on PCB
```
After that make the outer boundaries of PCB by selecting 'Draw circle' from item list and select 'Edge cut' and increase its size to 0.8mm.
![](./img/KiCad/edge.png)

It will be difficult to cut a circular edge circuit, therefore it is better to create another boundary in rectangle shape.
Now I have to cut layers. Rectangle is my first cut layer and all the top traces will be milled by the machine.
After cutting the rectangle, then flip and mill from the back side and cut the final circular layer.
![](./img/KiCad/doubleedge.png)

# Exporting file for manufacturing.

To export file for manufacturing, Go to file and select 'PORT'. Select plot format as SVG. select F.Cu, Negative plot, Plot edge cuts on all layers and select Drill Mark to actual size. Check zone files before plotting and click export.
![](./img/KiCad/port.png)

Then files will be saved in my local folder and if opened with inkscape I can see the through holes in my PCB.
![](./img/KiCad/throuh.png)


If we select the Drill mark to none, the circuit will be printed without through hole.
![](./img/KiCad/throughno.png)
Use above Pcb for the top layer. Select the circuit and rename it and export it.(toplayer.png)
Then open the saved circuit (in my case: toplayer.png) in GIMP and use bucket fill tool to fill the circular boundary with black color.
```
image mode: Indexed.
```
![](./img/KiCad/nobound.png)
Go to file and click on overwrite toplayer.png and close it.

# Drill and cut in same layer.
Open the circuit in Kicad and select Drill marks to actual size and plot it again. 

Open the above circuit in inkscape and export it by renaming the file. 
Then open the open the renamed file in GIMP. In GIMP I have to preserve the rectangle and through hole. To do that I changed the color of rectangle and hole using bucket fill tool to red and fill other with balck color.
Then export it. In this process the machine will mill the holes and the rectangle.
![](./img/KiCad/hole.png)

After milling the board now flip the board to mill the bottom layer. 
For that go back to KiCAD and change the layer to bottom layer and change drill mark to none and plot it.
![](./img/KiCad/bottom.png)

Open the bottom layer to in inkscape and select the circuit and export it.
now select the bottom layer from the folder and open it in GIMP.
Just like before go to mode, select index, select black and white and bucket fill the boundary with black.
![](./img/KiCad/b.layer.png)

Maintaining the position of hole is very important.
For that flip the board by selecting flip from the tool bar and overwrite on it from file.

## To cut the edge of pcb.
Keep only the circular boundary. Make all components black.
Finally invert it by using the invert tool from color menu.
As the circle is symmertic shape I don't have to flip it. If it's complicated shape we have to flip it.
export it by renaming the file.
![](./img/KiCad/final.png)

Thank You
---
---