# KiCAD
In todays session we have learnt about using KiCAD software for PCB design.

Before the session starts we made our KiCAD software ready. During the session we first made a project and I named it as fab00. Under the project two files were automatically created kicad.pcb and kicad.sch. We have to opened the kicad schematic and started adding parts using **add a symbol** button which on the right side. To search our parts we clicked on the search button and typed the name of the component.
In KiCAD we just use abstract symbols unlike specific symbols used in Eagle.

![search](./img/search.png)

There are different shortcut keys for specfic task. If we hover over the key buttons all the short keys were given.
```
For example to move the parts- M is used, to rotate parts -R is used, to add wire- W is used, to add components _A is used etc.. 
```


After insertion of the parts we can make connection between them by either draging one component end and joing to the other end of the component, but problem here is if we move the parts the connection will be disconnected. Therefore, the best thing is we can use the **add wire (W)** command to connect the wire
s.

To complete circuits we have to add ***VCC and GND.***

***Power_flag*** is used to notify that VCC and GND are the power source.

### **Renaming and Numbering of the components.**
To rename any components, click on the component and right click and go to properties in the dropdown menu. We can change the component name by typing the specific name in the value fields.
![search](./img/name.png)

To Nmber the components, click on tools and select annotate schematic. Inside annotate, choose the option how we wanted to annotate and click annotate button.

![search](./img/annotate.png)
![nember](./img/numbering.png)


**Below is the simple circuit that we have designed in the class.**

![schematic](./img/kicadclass.png)

# ASSIGNMENT: make a basic circuit board.
Component should use: 
```
1-Resistor, 2-led, 1-button.
```
I followed all the instruction given above and tried myself to build a circuit in KiCAD.
In these assignment I have used two led, the blue led will glow when my circuir is connected to power and the red led will glow only if I put on the switch.

![assign](./img/assign.png)

---
---

# MAKE PCB BOARD FROM SCHEMATIC.
 Before going to PCB editor assign **footprints** to each components. To assign the footprints got to tool box and click on the assign footprints and open the footprint viewr to view the parts.
 Then apply ,save schematic and continue.

 ![assign](./img/footprints.png)


 Then open the PCB in board editor window and pull the components from sechematic. For that click on 'Read netlist and update board connectivity'. Click on update and close it.

 ![assign](./img/update.png)

 Hover over the components and move the the parts for routing. For routing there are different modes to play with in route bar. Try once.


 ![assign](./img/route.png)

 ***OUTLINE DRAWING IN PCB*** 
 select 'draw a line' from selection item list and select edge cut.
 Cut the boundaries with line or use rectange to draw the boundries.

 We can even increase the boundary line by right clicking on the border and selecting the the properties to increase the line width to know the boundary interferance with any components inside the box beforehand. 
 Like in the above image we can measure board size and even view it in 3D by clicking on the view button.
 
 ![assign](./img/3dview.png)
 

 # ASSIGNMENT: PCB BOARD DESIGN AND ROUTING.

 I have made pcb board and routed it by following the above procedure.
 ![assign](./img/assignmentpcb.png)
 
 ![assign](./img/assign3d.png)

 ---
 ---