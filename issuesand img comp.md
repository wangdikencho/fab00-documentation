# Issues In Gitlab.

During this session we have learnt about how to create issues, labels, milestones and Boardes in gitlab.


## Issues:
The Issue Tracker is the place to add things that we need to improve or solve in a project
Issues can be bugs, tasks or ideas to be discussed. Also, issues are searchable and filterable.


![](./img/temp/issue1.png)

## Labels:

Label are like tags to indicate the status of an issue. We can create new labels as per our choice. I have created a new label called 'solved'.

![](./img/temp/label.png)

## Milestones:

Milestones allows us to organize issues and merge requests into a cohesive group, with an optional start date and an optional due date: [From GitLab Docs](https://docs.gitlab.com/ee/user/project/milestones/).

I created completion of fab zero and fab academy as my milestone.

![](./img/temp/milestone.png)

## Boards:

Issue tracker comes with only 2 Boards open and closed .When we complete our issues we can move our issue from open board to close. It means we have solved the issues.

![](./img/boards.png)

  As I have solved my first issue I moved it to close board. This means we can no longer see this issue in the our issue list.
  
  ---
  --
  
  # Flameshot
  I have downloaded the flameshot using command prompt.
  ```
winget install flameshot
  ```
 ![](./img/temp/flame.png)

  # Xn convert
  I have downloaded Xn corvet to compress my imange size.
  - To compress the image we can either select files or the entire the folder.
  - After selection go to add options and compressed yor images as per your will. 
  - Select image format and other options from output tab.
  - Convert the images by clicking on the convert options.
  - It will take only few seconds to convert your compressed images.
  


  ![](./img/temp/xn.png)

---
---