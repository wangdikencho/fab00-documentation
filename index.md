# Documentation for Fab ZERO.

## Project in GitLab.

### Operation System Used: WSL.


  To complie and document all the work and task done in fabzero program firstly, I have signed into my git lab account and I have created a project on the gitlab platform and named the folder fab00 Documentation. Initially I created  a folder 'fab0 documentation' in my git lab using git bash and later switched to WSL terminal. 

![](./img/project.png)

---

## How I have generated SSH key in Terminal.

```
 ssh-keygen -t ed25519 "kencho.cnr@rub.edu.bt"      
```
- when the above command is excuted on the terminal, this will help to generate ssh key for the client machine. The screenshot below presents output of the execution of the above command.

![](./img/ssh.png)

- This will generate a private and public ssh key for the machine. The keys were generate in the location indicated below.

```
/home/kencho/.ssh
```

 - There are two ssh key generated ie; id_rsa and id_rsa.pub and i have copied the public key and pasted to the gitlab account so that machine gets configured with git account.
  
![](./img/key.png)

- I have used git command 'ssh -T git@gitlab.com ' to Check if the machine is linked with my gitlab account.

        

- the output of the above command showed successful linkage of gitlab and the machine.

![](./img/config.png)

---
## Cloning The Project.

  After successful linkage, I have used git clone command to clone the Repository from gitlab to my labtop. The screenshot below shows the process of cloning to my "fab00 documentation directory".

![](./img/clone.png)

---
## Push the files to the Reprository.


 At first I have created an index file is created in my vs  and I did git commit and git with message "trying fist push". After that I have documented the assignment in Index file. I have created a about me file in the project and when all the works are done I did git commit and pushed the documents to the gitlab repository (faboo documentation). The screenshot below shows the process to git commit and git push the files to the repository.

![](./img/push.png)

- In my screenshot I pushed my docs. several times because I was not able paste my password correctly.

---