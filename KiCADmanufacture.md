# Making PCB ready for Manufacturing

- Go to files and click on Gerbers (.gbr), and choose the layers we want. 
- Click on Front Cu(F.Cu) layer and edge cut from include layer option. 
- From general options select only check zone fills before plotting and choose an output directory and create a new folder (i kept my folder name assignment.)
- we can also generate drill files
- click on plot and close it.

![](../img/Kicad/gerber.png)

# Mods and SRM 20
- go to file and click on plot
- plot format is same as gerber format.
- select the plot format to SVG.
- select the drill mark to actual size and set other options like in gerber, but don't forget to trick plot edge cut from general option and negative plot.
- click plot and it will create a svg file.
  
After that open the SVG file in inkscape.
select the circuit and go to document properties from file and resize page to content. 
When the circuit is opened in inkscape, select the circuit and select export png image from file. select the dpi to 1500, rename and click export.

![](../img/Kicad/plot.png)

## make layer for cutting.

To make layer for cutting
- go back to the folder where you have saved the EDGE cut board and open it in inkscape.
- select the edge /boundary and export it by changing the file name.
- then open the cut.png file from your folder in GIMP. to cut the circuit boundaries, select color option in GIMP and invert the color and save it.


# Invert image using MODS
search modsproject.org on internet and right click.
select programs and then open programs. select pcb absolute. 
then select the png file and then invert it like in the image shown below.
![](../img/Kicad/mods.png)

---
---